package com.devcamp.erdtoentity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.OrderDetail;

public interface IOrderDetailRepository extends JpaRepository<OrderDetail, Integer>{
    @Query(value = "SELECT * FROM  order_details WHERE order_id = :param1 AND product_id = :param2 ORDER BY order_id DESC",
         nativeQuery = true)
	List<OrderDetail> findOrderDetailByOrderIdAndProductId(@Param("param1") Integer param1, 
        @Param("param2") Integer param2);
}
