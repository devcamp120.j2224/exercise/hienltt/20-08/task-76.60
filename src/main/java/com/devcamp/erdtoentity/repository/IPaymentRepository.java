package com.devcamp.erdtoentity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.Payment;

public interface IPaymentRepository extends JpaRepository<Payment, Integer>{
    @Query(value = "SELECT * FROM  payments WHERE check_number = :param1", nativeQuery = true)
	List<Payment> findPaymentByCheckNumber(@Param("param1") String param1);
}
