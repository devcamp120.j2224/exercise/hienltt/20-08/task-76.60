package com.devcamp.erdtoentity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.Product;

public interface IProductRepository extends JpaRepository<Product, Integer>{
    @Query(value = "SELECT * FROM  products WHERE product_name LIKE %:param1% ", nativeQuery = true)
	List<Product> findProductByProductNameLike(@Param("param1") String param1);
}
