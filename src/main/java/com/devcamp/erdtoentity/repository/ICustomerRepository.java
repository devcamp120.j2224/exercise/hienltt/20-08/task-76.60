package com.devcamp.erdtoentity.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer,Integer>{

    @Query(value = "SELECT * FROM customers WHERE last_name LIKE %:paramLastname% OR first_name LIKE %:paramFirstname% ORDER BY last_name DESC",
         nativeQuery = true)
	List<Customer> findCustomerByFirstNameOrLastNameLike(@Param("paramLastname") String paramLastname, 
        @Param("paramFirstname") String paramFirstname);

    @Query(value = "SELECT * FROM customers WHERE city LIKE %:paramCity% OR state LIKE %:paramState% ORDER BY city DESC",
        nativeQuery = true)
    List<Customer> findCustomerByCityeOrStateLike(@Param("paramCity") String paramCity, 
       @Param("paramState") String paramState, Pageable pageable);   

    @Query(value = "SELECT * FROM customers WHERE country LIKE %:paramCountry% ORDER BY first_name ASC",
       nativeQuery = true)
    List<Customer> findCustomerByCountryWithPageable(@Param("paramCountry") String country, Pageable pageable);
  
    @Transactional
    @Modifying
    @Query(value = "UPDATE customers SET country = :paramCountry WHERE country = null", nativeQuery = true)
    int updateCustomerAtCountryNull(@Param("paramCountry") String paramCountry);
}
