package com.devcamp.erdtoentity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee, Integer>{
    @Query(value = "SELECT * FROM employees WHERE last_name LIKE %:paramLastname% OR first_name LIKE %:paramFirstname% ORDER BY last_name DESC",
         nativeQuery = true)
	List<Employee> findEmployeeByFirstNameOrLastNameLike(@Param("paramLastname") String paramLastname, 
        @Param("paramFirstname") String paramFirstname);
}
