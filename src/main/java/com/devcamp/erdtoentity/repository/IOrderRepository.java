package com.devcamp.erdtoentity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.Order;

public interface IOrderRepository extends JpaRepository<Order, Integer>{
    @Query(value = "SELECT * FROM  orders WHERE customer_id = :param1",
         nativeQuery = true)
	List<Order> findOrderByCustomerId(@Param("param1") Integer param1);
}

