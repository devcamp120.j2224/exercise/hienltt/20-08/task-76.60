package com.devcamp.erdtoentity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.Office;

public interface IOfficeRepository  extends JpaRepository<Office, Integer>{
    @Query(value = "SELECT * FROM offices WHERE state LIKE %:paramState% OR country LIKE %:paramCountry% ORDER BY country DESC",
         nativeQuery = true)
	List<Office> findOfficeByStateOrCountryLike(@Param("paramState") String paramState, 
        @Param("paramCountry") String paramCountry);
}
