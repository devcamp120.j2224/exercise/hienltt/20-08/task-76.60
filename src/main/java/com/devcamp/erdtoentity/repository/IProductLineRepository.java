package com.devcamp.erdtoentity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdtoentity.entity.ProductLine;

public interface IProductLineRepository extends JpaRepository<ProductLine, Integer>{
    @Query(value = "SELECT * FROM  product_lines WHERE product_line LIKE %:param1% ", nativeQuery = true)
	List<ProductLine> findProductLineLike(@Param("param1") String param1);
}
