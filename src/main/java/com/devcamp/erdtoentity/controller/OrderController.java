package com.devcamp.erdtoentity.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.Customer;
import com.devcamp.erdtoentity.entity.Order;
import com.devcamp.erdtoentity.repository.ICustomerRepository;
import com.devcamp.erdtoentity.repository.IOrderRepository;

@RestController
@CrossOrigin
public class OrderController {
    @Autowired
    private IOrderRepository iOrderRepository;
    @Autowired
    private ICustomerRepository iCustomerRepository;

    @GetMapping("/orders")
    public List<Order> getAllOrder(){
        return iOrderRepository.findAll();
    }

    @GetMapping("/orders/{orderId}")
    public ResponseEntity<Order> getByOrderId(@PathVariable("orderId") int orderId){
        try {
            Optional<Order> checkOrder = iOrderRepository.findById(orderId);
            if(checkOrder.isPresent()){
                return new ResponseEntity<Order>(checkOrder.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customers/{customerId}/orders")
    public ResponseEntity<Object> createNewOrder(@PathVariable("customerId") int customerId, @RequestBody Order newOrder){
        try {
            Optional<Customer> checkCustomer = iCustomerRepository.findById(customerId);
            if(checkCustomer.isPresent()){
                Order _order = new Order();
                _order.setComments(newOrder.getComments());
                _order.setOrderDate(new Date());
                _order.setStatus(newOrder.getStatus());
                _order.setCustomer(checkCustomer.get());
                _order.setRequiredDate(new Date());
                _order.setShippedDate(null);
                return new ResponseEntity<Object>(iOrderRepository.save(_order), HttpStatus.CREATED);
            } else {
                return ResponseEntity.unprocessableEntity().body("Không tìm thấy Customer");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/orders/{orderId}")
    public ResponseEntity<Object> updateByOrderId(@PathVariable("orderId") int orderId, @RequestBody Order order){
        try {
            Optional<Order> checkOrder = iOrderRepository.findById(orderId);
            if(checkOrder.isPresent()){
                Order _order = checkOrder.get();
                _order.setComments(order.getComments());
                _order.setStatus(order.getStatus());
                _order.setShippedDate(order.getShippedDate());
                return new ResponseEntity<Object>(iOrderRepository.save(_order), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/orders/{orderId}")
    public ResponseEntity<Order> deleteByOrderId(@PathVariable("orderId") int orderId){
        try {
            iOrderRepository.deleteById(orderId);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //tìm danh sách theo customerId
    @GetMapping("/order")
    public ResponseEntity<List<Order>> findOrderByCustomerId(@RequestParam("customerId") Integer customerId){
        try {
            List<Order> list = iOrderRepository.findOrderByCustomerId(customerId);
            if(list != null){
                return new ResponseEntity<List<Order>>(list, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
