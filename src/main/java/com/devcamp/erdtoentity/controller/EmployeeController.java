package com.devcamp.erdtoentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.Employee;
import com.devcamp.erdtoentity.repository.IEmployeeRepository;

@RestController
@CrossOrigin
public class EmployeeController {
    @Autowired
    private IEmployeeRepository iEmployeeRepository;

    @GetMapping("/employees")
    public List<Employee> getAllEmployee(){
        return iEmployeeRepository.findAll();
    }

    @GetMapping("/employees/{employeeId}")
    public ResponseEntity<Employee> getByEmployeeId(@PathVariable("employeeId") int employeeId){
        try {
            Optional<Employee> checkEmployee = iEmployeeRepository.findById(employeeId);
            if(checkEmployee.isPresent()){
                return new ResponseEntity<Employee>(checkEmployee.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employees")
    public ResponseEntity<Employee> createNewEmployee(@RequestBody Employee newEmployee){
        try {
            Employee _employee = new Employee();
            _employee.setFirstName(newEmployee.getFirstName());
            _employee.setLastName(newEmployee.getLastName());
            _employee.setExtension(newEmployee.getExtension());
            _employee.setEmail(newEmployee.getEmail());
            _employee.setOfficeCode(newEmployee.getOfficeCode());
            _employee.setReportTo(newEmployee.getReportTo());
            _employee.setJobTitle(newEmployee.getJobTitle());
            return new ResponseEntity<Employee>(iEmployeeRepository.save(_employee), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/employees/{employeeId}")
    public ResponseEntity<Employee> updateByEmployeeId(@PathVariable("employeeId") int employeeId, @RequestBody Employee employee){
        try {
            Optional<Employee> checkEmployee = iEmployeeRepository.findById(employeeId);
            if(checkEmployee.isPresent()){
                Employee _employee = checkEmployee.get();
                _employee.setFirstName(employee.getFirstName());
                _employee.setLastName(employee.getLastName());
                _employee.setExtension(employee.getExtension());
                _employee.setEmail(employee.getEmail());
                _employee.setOfficeCode(employee.getOfficeCode());
                _employee.setReportTo(employee.getReportTo());
                _employee.setJobTitle(employee.getJobTitle());
                return new ResponseEntity<Employee>(iEmployeeRepository.save(_employee), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/employees/{employeeId}")
    public ResponseEntity<Employee> deleteByCustomerId(@PathVariable("employeeId") int employeeId){
        try {
            iEmployeeRepository.deleteById(employeeId);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //tìm danh sách theo họ hoặc tên với LIKE
    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> findEmployeeByFirstNameOrLastNameLike(@RequestParam("firstname") String firstname,
            @RequestParam("lastname") String lastname){
        try {
            List<Employee> list = iEmployeeRepository.findEmployeeByFirstNameOrLastNameLike(lastname, firstname);
            if(list != null){
                return new ResponseEntity<List<Employee>>(list, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
