package com.devcamp.erdtoentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.Customer;
import com.devcamp.erdtoentity.repository.ICustomerRepository;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    private ICustomerRepository iCustomerRepository;

    @GetMapping("/customers")
    public List<Customer> getAllCustomer(){
        return iCustomerRepository.findAll();
    }

    @GetMapping("/customers/{customerId}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable("customerId") int customerId){
        try {
            Optional<Customer> checkCustomer = iCustomerRepository.findById(customerId);
            if(checkCustomer.isPresent()){
                return new ResponseEntity<Customer>(checkCustomer.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customers")
    public ResponseEntity<Customer> createNewCustomer(@RequestBody Customer newCustomer){
        try {
            Customer _customer = new Customer();
            _customer.setFirstName(newCustomer.getFirstName());
            _customer.setLastName(newCustomer.getLastName());
            _customer.setPhoneNumber(newCustomer.getPhoneNumber());
            _customer.setAddress(newCustomer.getAddress());
            _customer.setCity(newCustomer.getCity());
            _customer.setState(newCustomer.getState());
            _customer.setPostalCode(newCustomer.getPostalCode());
            _customer.setCountry(newCustomer.getCountry());
            _customer.setSalesRepEmployeeNumber(newCustomer.getSalesRepEmployeeNumber());
            _customer.setCreditLimit(newCustomer.getCreditLimit());
            return new ResponseEntity<Customer>(iCustomerRepository.save(_customer), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/customers/{customerId}")
    public ResponseEntity<Customer> updateByCusTomerId(@PathVariable("customerId") int customerId, @RequestBody Customer customer){
        try {
            Optional<Customer> checkCustomer = iCustomerRepository.findById(customerId);
            if(checkCustomer.isPresent()){
                Customer _customer = checkCustomer.get();
                _customer.setFirstName(customer.getFirstName());
                _customer.setLastName(customer.getLastName());
                _customer.setPhoneNumber(customer.getPhoneNumber());
                _customer.setAddress(customer.getAddress());
                _customer.setCity(customer.getCity());
                _customer.setState(customer.getState());
                _customer.setPostalCode(customer.getPostalCode());
                _customer.setCountry(customer.getCountry());
                _customer.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
                _customer.setCreditLimit(customer.getCreditLimit());
                return new ResponseEntity<Customer>(iCustomerRepository.save(_customer), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<Customer> deleteByCustomerId(@PathVariable("customerId") int customerId){
        try {
            iCustomerRepository.deleteById(customerId);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //tìm danh sách theo họ hoặc tên với LIKE
    @GetMapping("/customers/{firstname}/{lastname}")
    public ResponseEntity<List<Customer>> findCustomerByFirstNameOrLastNameLike(@PathVariable("firstname") String firstname,
            @PathVariable("lastname") String lastname){
        try {
            List<Customer> list = iCustomerRepository.findCustomerByFirstNameOrLastNameLike(lastname, firstname);
            if(list != null){
                return new ResponseEntity<List<Customer>>(list, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //tìm danh sách theo city, state với LIKE có phân trang.
    @GetMapping("/customers/city={city}/state={state}")
    public ResponseEntity<List<Customer>> findCustomerByCityeOrStateLike(@PathVariable("city") String city,
            @PathVariable("state") String state){
        try {
            int lenght = 10;
			int start = 0;
            List<Customer> list = iCustomerRepository.findCustomerByCityeOrStateLike(city, state, PageRequest.of(start, lenght));
            if(list != null){
                return new ResponseEntity<List<Customer>>(list, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //tìm danh sách theo country có phân trang và ORDER BY tên
    @GetMapping("/customers/country={country}")
    public ResponseEntity<List<Customer>> findCustomerByCountryWithPageable(@PathVariable("country") String country){
        try {
            int lenght = 5;
			int start = 0;
            List<Customer> list = iCustomerRepository.findCustomerByCountryWithPageable(country, PageRequest.of(start, lenght));
            if(list != null){
                return new ResponseEntity<List<Customer>>(list, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //UPDATE dữ liệu có country = NULL với giá trị truyền vào từ tham số
    @PutMapping("/customers/updateCountry")
    public ResponseEntity<Object> updateCustomerAtCountryNull(@RequestParam("country") String country){
        try {
            iCustomerRepository.updateCustomerAtCountryNull(country);
           
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
