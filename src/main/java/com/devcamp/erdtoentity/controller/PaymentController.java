package com.devcamp.erdtoentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.Customer;
import com.devcamp.erdtoentity.entity.Payment;
import com.devcamp.erdtoentity.repository.ICustomerRepository;
import com.devcamp.erdtoentity.repository.IPaymentRepository;

@RestController
@CrossOrigin
public class PaymentController {
    @Autowired
    private IPaymentRepository iPaymentRepository;
    @Autowired
    private ICustomerRepository iCustomerRepository;

    @GetMapping("/payments")
    public List<Payment> getAllEmployee(){
        return iPaymentRepository.findAll();
    }

    @GetMapping("/payments/{paymentId}")
    public ResponseEntity<Payment> getByPaymentId(@PathVariable("paymentId") int paymentId){
        try {
            Optional<Payment> checkPayment = iPaymentRepository.findById(paymentId);
            if(checkPayment.isPresent()){
                return new ResponseEntity<Payment>(checkPayment.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customers/{customerId}/payment")
    public ResponseEntity<Object> createNewOrder(@PathVariable("customerId") int customerId, @RequestBody Payment newPayment){
        try {
            Optional<Customer> checkCustomer = iCustomerRepository.findById(customerId);
            if(checkCustomer.isPresent()){
                Payment _payment = new Payment();
                _payment.setAmmount(newPayment.getAmmount());
                _payment.setCheckNumber(newPayment.getCheckNumber());
                _payment.setPaymentDate(newPayment.getPaymentDate());
                _payment.setCustomer(checkCustomer.get());
                return new ResponseEntity<Object>(iPaymentRepository.save(_payment), HttpStatus.CREATED);
            } else {
                return ResponseEntity.unprocessableEntity().body("Không tìm thấy Customer");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/payments/{paymentId}")
    public ResponseEntity<Object> updateByPaymentId(@PathVariable("paymentId") int paymentId, @RequestBody Payment payment){
        try {
            Optional<Payment> check = iPaymentRepository.findById(paymentId);
            if(check.isPresent()){
                Payment _payment = check.get();
                _payment.setAmmount(payment.getAmmount());
                _payment.setCheckNumber(payment.getCheckNumber());
                _payment.setPaymentDate(payment.getPaymentDate());
                return new ResponseEntity<Object>(iPaymentRepository.save(_payment), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/payments/{paymentId}")
    public ResponseEntity<Payment> deleteByPaymnetId(@PathVariable("paymentId") int paymentId){
        try {
            iPaymentRepository.deleteById(paymentId);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //tìm danh sách theo check_number
    @GetMapping("/payment")
    public ResponseEntity<List<Payment>> findPaymentByCheckNumber(@RequestParam("checkNumber") String checkNumber){
        try {
            List<Payment> list = iPaymentRepository.findPaymentByCheckNumber(checkNumber);
            if(list != null){
                return new ResponseEntity<List<Payment>>(list, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
